import { IsNotEmpty, IsPositive } from 'class-validator';

class createOrderItemDTO {
  @IsNotEmpty()
  @IsPositive()
  productId: number;

  @IsNotEmpty()
  @IsPositive()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  @IsPositive()
  customerId: number;

  @IsNotEmpty()
  orderItem: createOrderItemDTO[];
}
